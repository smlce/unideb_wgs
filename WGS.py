#!/bin/python

### Based on scripts by Tibor Nagy!


## Alignment and haplotyping, GVCF generation (hg38)

%%bash
REF=/path/to/genomes/gatkboundle/Homo_sapiens_assembly38.fasta
BUNDLE=/path/to/genomes/gatkboundle


for FQ1 in *_1.fastq.gz                                                                                                                                                                                                                     
do                                                                                                                                                                                                                                          
        FQ2=${FQ1%_1.fastq.gz}_2.fastq.gz                                                                                                                                                                                                   
        bwa mem $REF $FQ1 $FQ2                                                                                                                                                                                                            
done | samtools view -b - >$ALIGN

# List the names of your bam files here!
for BAM in GM22647.bam GM22648.bam GM22649.bam GM22650.bam GM22651.bam
do
  gatk --java-options '-Xmx20G' AddOrReplaceReadGroups -I $BAM -O ${BAM%bam}rg.bam -LB lib1 -PL illumina -PU unit1 -SM ${BAM%.bam} -ID ${BAM%.bam} --VALIDATION_STRINGENCY LENIENT
  gatk --java-options '-Xmx20G' MarkDuplicates -I ${BAM%bam}rg.bam -O ${BAM%bam}md.bam -M ${BAM%bam}metrics
  gatk --java-options '-Xmx20G' BaseRecalibrator -I ${BAM%bam}md.bam -O ${BAM%bam}table -R $REF --known-sites /data/genomes/gatkboundle/dbsnp_146.hg38.vcf.gz --known-sites ${BUNDLE}/1000G_phase1.snps.high_confidence.hg38.vcf.gz
  gatk --java-options '-Xmx20G' ApplyBQSR -I ${BAM%bam}md.bam -O ${BAM%bam}bc.bam -R $REF --bqsr-recal-file ${BAM%bam}table
  rm ${BAM%bam}rg.bam ${BAM%bam}md.bam ${BAM%bam}table ${BAM%bam}metrics
  gatk --java-options '-Xmx20G' HaplotypeCaller -I ${BAM%bam}bc.bam -R $REF -O ${BAM%bam}gvcf.vcf.gz -ERC GVCF
done


# GVCF postprocessing, filtering of variations, recalibration of scores (hg38)

BUNDLE=/path/to/genomes/gatkboundle
REF=$BUNDLE/Homo_sapiens_assembly38.fasta
PREFIX=mydata

# List the names of your GVCF files here with the -V option!
gatk --java-options "-Xmx20g -Xms4g" CombineGVCFs -V GM22647.gvcf.vcf.gz -V GM22648.gvcf.vcf.gz -V GM22649.gvcf.vcf.gz -V GM22650.gvcf.vcf.gz -V GM22651.gvcf.vcf.gz -O raw.gvcf.gz -R $REF
gatk --java-options "-Xmx20g" GenotypeGVCFs -R $REF -V raw.gvcf.gz -O ${PREFIX}.raw.vcf.gz
gatk --java-options "-Xmx20g" VariantRecalibrator -R $REF -V ${PREFIX}.raw.vcf.gz --resource:hapmap,known=false,training=true,truth=true,prior=15.0 $BUNDLE/hapmap_3.3.hg38.vcf.gz --resource:omni,known=false,training=true,truth=false,prior=12.0 $BUNDLE/1000G_omni2.5.hg38.vcf.gz --resource:1000G,known=false,training=true,truth=false,prior=10.0 $BUNDLE/1000G_phase1.snps.high_confidence.hg38.vcf.gz --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $BUNDLE/dbsnp_146.hg38.vcf.gz --tranches-file ${PREFIX}.snp.tr -O ${PREFIX}.snp.recal -mode SNP -an QD -an MQ -an MQRankSum -an FS -an SOR -an DP -an ReadPosRankSum
gatk --java-options "-Xmx20g" VariantRecalibrator -R $REF -V ${PREFIX}.raw.vcf.gz --resource:mills,known=false,training=true,truth=true,prior=12.0 $BUNDLE/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $BUNDLE/dbsnp_146.hg38.vcf.gz --tranches-file ${PREFIX}.indel.tr -O ${PREFIX}.indel.recal -mode INDEL -an QD -an MQRankSum -an FS -an SOR -an DP -an ReadPosRankSum --max-gaussians 5
gatk --java-options "-Xmx20g" ApplyVQSR -R $REF -V ${PREFIX}.raw.vcf.gz -O ${PREFIX}.snpready.vcf.gz --recal-file ${PREFIX}.snp.recal --tranches-file ${PREFIX}.snp.tr --mode SNP --truth-sensitivity-filter-level 99.5
gatk --java-options "-Xmx20g" ApplyVQSR -R $REF -V ${PREFIX}.snpready.vcf.gz -O ${PREFIX}.vcf.gz --recal-file ${PREFIX}.indel.recal --tranches-file ${PREFIX}.indel.tr --mode INDEL --truth-sensitivity-filter-level 99.0


