# Unideb_WGS

WGS pipeline for the BBL group at UD.

## Introduction

This is a WGS pipeline based on Tibor Nagy's scripts, which expects to start from Illumina sequenced, filtered fastq files, and it will perform the alignment to the genome and identifying the variations, including various filterings and score recalibrations, and generating VCF files.

## Installation and setup

This a single python script pipeline. Installation is as easy as cloning the the repository with git to your local computer. Check if you have privileges to write and to run the script. Open the script, and change the values of the variables "REF", "BUNDLE" and "PREFIX" to match your paths and naming preferences. From then on the pipeline can run automatically.

However, the script has some prerequisites before it is ready to run, please check the next chapter.

### Prerequisites

To properly run the script, you need to have certain software tools and indexes ready on your computer. The software tools should be on your PATH so that the script can call them wherever hey are actually installed. The indexes are need to be in the "index_dir" directory.

### Software tools

The following software are necessary to run the script (in brackets you can find the version this pipeline was tested with; newer versions should be backward compatible):

- Python (3.9.1)
- BWA (0.7.17)
- GATK (4.2.4.1)

Please check the project pages and the manuals of the respective software tools for installation and setup instructions. Be aware that I am not maintaining these software, for troubleshooting please contact their respective support contacts.

### Indexes

The script is currently recommended to process hg38 samples, but other organisms and versions can be added too by using a different index. Acquire the appropriate files from a reliable database, e.g. Ensembl. The following files are needed:

- BWA: genome file in multifasta format
- GATK: genome file in multifasta format

Note that GATK comes with a bundle of data files that you will also use, these are automatically installed with the rest of the software.  
Please follow the manuals of the respective software tools to prepare the genome indexes.

### Additional setup

In the script you need to list the names of your bam files and your GVCF files. Note that the script is always working in the current folder, all the results files you generate (alignments, haplotypes etc.) will be in the current working directory. Your initial fastq files must also be present in this directory.

## Running the script

Once everything is set up properly, you can run the script as a python script (it is written in python, although it will use the bash shell for certain steps).

`python WGS.py`

There are no command line parameters as everything is hard coded into the script.

The script performs the following steps:

1. Aligning the reads to the genome with BWA (outputs sam files by default, samtools is used to sort and convert sam files to bam files)
2. Using GATK to add read groups
3. Using GATK to mark duplicate reads
4. Using GATK to recalibrate base scores around known variation sites
5. Using GATK to call haplotypes (GVCF files)
6. Using GATK to combine the GVCF files
7. Using GATK to call the variants
8. Using GATK to recalibrate the variant calling scores around known variants, both for SNPs and INDELs


## Output and downstream analyses

The pipeline will create the following outputs from the aforementioned steps: alignment files, intermediary bam files, GVCF files (haplotyping), intermediary VCF files, VCF files (genotyping).  
The most relevant for the downstream analyses are the VCF files, which contain information about the variants in the genomes. You can filter, sort them and compare them between samples, identifying new mutations that only occur in one sample group. VCF files can also be visualized, and variants can be annotated as well, to see which genes are affected by mutations. There are several software dsuits and packages that offer various visualization, filtering, sorting, scoring, annotation and comparison options, including GATK, vcfR, Vcflib, VCFtools and VCF-Miner.  
Once you annotated the variants, the affected genes/proteins can be subjected to ontology/pathway analysis, to determine which signalling pathways, cell components, interactions etc. they affect. There are several tools available for ontology and pathway analysis, including Reactome, DAVID and STRING.  
There are countless possibilities for downstream analyses, to get an overview of the full WGS data analysis process and get inspired, please have a look at the SequencEnG website: SequencEnG

## Troubleshooting

For any bugs found feel free to create an issue in this project page.
